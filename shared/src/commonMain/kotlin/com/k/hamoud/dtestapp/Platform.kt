package com.k.hamoud.dtestapp

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform